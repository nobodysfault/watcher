# watcher

Android build-tools / support libraries watcher. 
Sends email when new version is available.

## Usage

1. Update `resources/config.edn`
1. Make a jar file 
    
    `$ lein uberjar`
    
1. Run
    
    `$ java -jar watcher-1.0-standalone.jar`
