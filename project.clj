(defproject watcher "1.0"
  :description "Android build tools / support library watcher, sends email when new version is available"
  :url "https://bitbucket.org/nobodysfault/watcher"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [hickory "0.7.1"]
                 [clj-http "3.7.0"]
                 [yogthos/config "0.8"]
                 [com.draines/postal "2.0.2"]]
  :main ^:skip-aot watcher.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
