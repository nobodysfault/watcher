(ns watcher.core
  (:require [config.core :refer [env]])
  (:gen-class))

(use
 'hickory.core)
(use
 'hickory.render)
(use
 'postal.core)

(require
 '[clj-http.client :as client])
(require
 '[clojure.string :as string])
(require
 '[hickory.select :as select])

; Build tools declaration
(def build-tools-url
  "https://developer.android.com/studio/releases/build-tools.html")
(def build-tools-selector (select/child (select/class "toggle-content")))
(def build-tools-title-selector (select/child (select/tag :p) (select/tag :a)))
(def build-tools-filename "build-tools.txt")

(defn is-build-tools-string?
  [x]
  (string/starts-with? x "Build Tools"))

(defn get-build-tools-description
  [src]
  (-> (select/select (select/child (select/class "toggle-content-toggleme")) src) first hickory-to-html))

; Support libraries declaration
(def support-libraries-url
  "https://developer.android.com/topic/libraries/support-library/revisions.html")
(def support-libraries-selector
  (select/child (select/class "jd-descr")))
(def support-libraries-title-selector
  (select/child
   (select/and (select/tag :h2)
               (select/attr :id #(not (string/blank? %))))))
(def support-libraries-filename "support-libraries.txt")

(defn get-support-libraries-description
  [src]
  (->>
   (select/select
    (select/child
     (select/and
      (select/follow
       (select/and (select/nth-of-type 1 :h2)
                   (select/attr :id #(not (string/blank? %))))
       select/any)
      (select/precede
       select/any
       (select/and (select/nth-of-type 2 :h2)
                   (select/attr :id #(not (string/blank? %)))))))
    src)
   (map #(hickory-to-html %),,,)
   (reduce str,,,)))

(defn is-support-libraries-string?
  [x]
  true)

(defn get-url-tree
  [url]
  (-> (client/get url)
      :body
      parse
      as-hickory))

(defn get-title
  [src selector]
  (-> (select/select selector src) first :content))

(defn latest-title-description
  [url
   latest-block-selector
   title-selector
   get-description
   title-string-filter]
  (let [latest-block
        (-> (select/select latest-block-selector (get-url-tree url))
            first)]
    (vector
     (->> (get-title latest-block title-selector)
          (filter string?,,,)
          (filter title-string-filter,,,)
          first)
     (get-description latest-block))))

(defn get-latest-saved-version
  [filename]
  (try
    (slurp filename)
    (catch java.io.FileNotFoundException _ nil)))

(defn send-new-version-email
  [subject body]
  (send-message
   {:host (:sender-smtp-server env)
    :user (:sender-username env)
    :pass (:sender-password env)
    :ssl  (:use-ssl env)}
   {:from    (:from env)
    :to      (:recipient-list env)
    :subject subject
    :body    [{:type    "text/html"
               :content body}]}))

(defn test-build-tools
  []
  (let [saved     (get-latest-saved-version build-tools-filename)
        real-pair (latest-title-description
                   build-tools-url
                   build-tools-selector
                   build-tools-title-selector
                   get-build-tools-description
                   is-build-tools-string?)
        real      (first real-pair)]
    (when-not (= saved real)
      (spit build-tools-filename real)
      (send-new-version-email (str (:new-build-tools-subject env) real) (second real-pair)))))

(defn test-support-libraries
  []
  (let [saved     (get-latest-saved-version support-libraries-filename)
        real-pair (latest-title-description
                   support-libraries-url
                   support-libraries-selector
                   support-libraries-title-selector
                   get-support-libraries-description
                   is-support-libraries-string?)
        real      (first real-pair)]
    (when-not (= saved real)
      (spit support-libraries-filename real)
      (send-new-version-email (str (:new-support-library-subject env) real) (second real-pair)))))

(defn -main
  [& args]
  (do
    (test-build-tools)
    (test-support-libraries)))
